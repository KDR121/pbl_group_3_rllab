#!/usr/bin/env python

from __future__ import print_function

import rospy
from pbl_demonstration.srv import TargetDirection,TargetDirectionResponse


def handle_target_direction(req):
    # print(req.circle_pos)
    cur_pos_x = req.circle_pos.circles[0].x
    cur_pos_y = req.circle_pos.circles[0].y
    next_direction = []
    limit_x = 0.1
    limit_y = 0.05

    if cur_pos_x <= -limit_x:
        # print("\nToo right. x:", cur_pos_x)
        next_direction.append('left')
    elif cur_pos_x >= limit_x:
        # print("\nToo left. x:", cur_pos_x)
        next_direction.append('right')
    else:
        # print("\nIn center. x:", cur_pos_x)
        next_direction.append("center")

    if cur_pos_y <= -limit_y:
        # print("Too high. y:", cur_pos_y)
        next_direction.append('up')
    elif cur_pos_y >= limit_y:
        # print("Too low. y:", cur_pos_y)
        next_direction.append('down')
    else:
        # print("In center. y:", cur_pos_y)
        next_direction.append("center")
    
    print(next_direction)
    return [next_direction]

def targetDirectionServer():
    rospy.init_node("target_direction")
    s = rospy.Service("target_direction", TargetDirection, handle_target_direction)
    rospy.spin()

if __name__=="__main__":
    targetDirectionServer()